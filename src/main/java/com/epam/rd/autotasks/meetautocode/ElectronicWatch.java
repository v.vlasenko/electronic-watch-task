package com.epam.rd.autotasks.meetautocode;

import java.util.Scanner;

public class ElectronicWatch {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int seconds = scanner.nextInt();

        int HH = (seconds / 3600) % 24;
        int MM = (seconds % 3600) / 60;
        int SS = (seconds % 3600) % 60;
        String timeInHHMMSS = String.format("%d:%02d:%02d", HH, MM, SS);
        System.out.println(timeInHHMMSS);

    }
}
